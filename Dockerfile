FROM strapi/base:12-alpine

ENV NODE_ENV production
WORKDIR /srv/app

RUN npm install -g strapi@v3.0.0-beta.18.8

COPY package*.json /srv/app/
RUN npm ci --only=production

COPY . /srv/app
RUN npm run build

CMD ["strapi", "start"]
