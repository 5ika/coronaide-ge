import React from "react";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";

const PostCodes = ({ value, setValue, setNp }) => {
  return (
    <Select
      value={value}
      onChange={e => {
        setValue(e.target.value);
        setNp(nps.find(i => i.id === e.target.value)?.np ?? "0000");
      }}
      autoWidth
      variant="outlined"
      displayEmpty
      style={{ width: "100%" }}
    >
      {nps &&
        [{ id: "", np: "Code postal" }, ...nps.sort((a, b) => a.np - b.np)].map(
          item => (
            <MenuItem key={item.id} value={item.id}>
              {item.np}
            </MenuItem>
          )
        )}
    </Select>
  );
};

export default PostCodes;

const nps = [
  {
    np: "1201",
    id: "5e72665073f2050970f7b141"
  },
  {
    np: "1202",
    id: "5e72665073f2055c0cf7b142"
  },
  {
    np: "1203",
    id: "5e72665073f205fbc7f7b143"
  },
  {
    np: "1204",
    id: "5e72665073f205c038f7b144"
  },
  {
    np: "1205",
    id: "5e72665073f2059ccdf7b145"
  },
  {
    np: "1206",
    id: "5e72665073f2051af1f7b146"
  },
  {
    np: "1207",
    id: "5e72665073f2053077f7b147"
  },
  {
    np: "1208",
    id: "5e72665073f2057cf9f7b148"
  },
  {
    np: "1214",
    id: "5e72665173f2055cf2f7b14a"
  },
  {
    np: "1217",
    id: "5e72665173f2057feff7b14b"
  },
  {
    np: "1219",
    id: "5e72665173f2056958f7b14c"
  },
  {
    np: "1228",
    id: "5e72665173f205f8a5f7b14d"
  },
  {
    np: "1290",
    id: "5e72665173f2056bfcf7b14e"
  },
  {
    np: "1209",
    id: "5e72665173f20547c8f7b14f"
  },
  {
    np: "1212",
    id: "5e72665173f20500b4f7b150"
  },
  {
    np: "1213",
    id: "5e72665173f205abbdf7b151"
  },
  {
    np: "1216",
    id: "5e72665173f205da3ef7b152"
  },
  {
    np: "1218",
    id: "5e72665273f205a9ecf7b153"
  },
  {
    np: "1219",
    id: "5e72665273f2058661f7b154"
  },
  {
    np: "1219",
    id: "5e72665273f205254ef7b155"
  },
  {
    np: "1220",
    id: "5e72665273f205a795f7b156"
  },
  {
    np: "1222",
    id: "5e72665273f20535def7b157"
  },
  {
    np: "1223",
    id: "5e72665273f20560caf7b158"
  },
  {
    np: "1224",
    id: "5e72665373f205cf2df7b159"
  },
  {
    np: "1225",
    id: "5e72665373f205d48df7b15a"
  },
  {
    np: "1226",
    id: "5e72665373f205f8f4f7b15b"
  },
  {
    np: "1227",
    id: "5e72665373f2054ca4f7b15c"
  },
  {
    np: "1231",
    id: "5e72665373f2056394f7b15e"
  },
  {
    np: "1232",
    id: "5e72665373f205da32f7b15f"
  },
  {
    np: "1233",
    id: "5e72665373f2050775f7b160"
  },
  {
    np: "1234",
    id: "5e72665373f2056088f7b161"
  },
  {
    np: "1236",
    id: "5e72665473f2051071f7b162"
  },
  {
    np: "1237",
    id: "5e72665473f2054d82f7b163"
  },
  {
    np: "1239",
    id: "5e72665473f205d77af7b164"
  },
  {
    np: "1241",
    id: "5e72665473f205004bf7b165"
  },
  {
    np: "1242",
    id: "5e72665473f205cfa7f7b166"
  },
  {
    np: "1243",
    id: "5e72665473f2055296f7b167"
  },
  {
    np: "1244",
    id: "5e72665473f2053e41f7b168"
  },
  {
    np: "1245",
    id: "5e72665473f205bd1cf7b169"
  },
  {
    np: "1247",
    id: "5e72665473f2052beaf7b16a"
  },
  {
    np: "1248",
    id: "5e72665473f20584d7f7b16b"
  },
  {
    np: "1251",
    id: "5e72665573f205b524f7b16c"
  },
  {
    np: "1252",
    id: "5e72665573f205f373f7b16d"
  },
  {
    np: "1253",
    id: "5e72665573f2055d6bf7b16e"
  },
  {
    np: "1254",
    id: "5e72665573f20583c9f7b16f"
  },
  {
    np: "1255",
    id: "5e72665673f2056d1cf7b170"
  },
  {
    np: "1256",
    id: "5e72665673f205e95df7b171"
  },
  {
    np: "1257",
    id: "5e72665673f20573adf7b172"
  },
  {
    np: "1258",
    id: "5e72665673f205a259f7b173"
  },
  {
    np: "1281",
    id: "5e72665673f2058151f7b174"
  },
  {
    np: "1284",
    id: "5e72665673f205a577f7b176"
  },
  {
    np: "1286",
    id: "5e72665673f2056737f7b177"
  },
  {
    np: "1287",
    id: "5e72665673f205021df7b178"
  },
  {
    np: "1288",
    id: "5e72665673f205c7b2f7b179"
  },
  {
    np: "1292",
    id: "5e72665773f205a634f7b17a"
  },
  {
    np: "1293",
    id: "5e72665773f205eb3cf7b17b"
  },
  {
    np: "1294",
    id: "5e72665773f2050ac1f7b17c"
  },
  {
    np: "1298",
    id: "5e72665773f2053dc6f7b17d"
  },
  {
    np: "1246",
    id: "5e72665773f2057612f7b17f"
  },
  {
    np: "1283",
    id: "5e72665773f20521e9f7b180"
  },
  {
    np: "1200",
    id: "5e72665773f2056b2df7b181"
  },
  {
    np: "1211",
    id: "5e72665873f20514fef7b182"
  },
  {
    np: "1215",
    id: "5e72665873f2052e95f7b183"
  },
  {
    np: "1240",
    id: "5e72665873f205118bf7b184"
  },
  {
    np: "1285",
    id: "5e72665873f205cc6ff7b185"
  }
];
