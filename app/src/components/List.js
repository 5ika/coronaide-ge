import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Typo from "@material-ui/core/Typography";
import Lst from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import IconButton from "@material-ui/core/IconButton";
import Button from "@material-ui/core/Button";
import Icon from "@material-ui/core/Icon";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import ListItemText from "@material-ui/core/ListItemText";

const useStyles = makeStyles(theme => ({
  list: {
    marginTop: "2rem",
    marginBottom: "2rem"
  },
  inputLine: {
    display: "flex",
    alignItems: "center"
  },
  button: {
    height: "56px",
    borderTopLeftRadius: 0,
    borderBottomLeftRadius: 0
  }
}));

const List = ({ list, setList }) => {
  const classes = useStyles();
  const [value, setValue] = useState("");

  const onAdd = () => {
    if (value && value !== "") {
      setList([...list, value]);
      setValue("");
    }
  };

  return (
    <div className={classes.list}>
      <Typo>Liste des besoins:</Typo>
      <Lst dense>
        {list &&
          list.map((item, idx) => (
            <ListItem key={idx}>
              <ListItemText>{item}</ListItemText>
              <ListItemSecondaryAction>
                <IconButton
                  edge="end"
                  onClick={() => {
                    const items = list.filter(i => i !== item);
                    setList(items);
                  }}
                >
                  <Icon>delete</Icon>
                </IconButton>
              </ListItemSecondaryAction>
            </ListItem>
          ))}
      </Lst>
      <div className={classes.inputLine}>
        <TextField
          label="Nouvel item"
          fullWidth
          variant="outlined"
          value={value}
          onChange={e => setValue(e.target.value)}
          // onKeyDown={e => {
          //   if (e.keyCode === 13) onAdd();
          // }}
        />
        <Button
          onClick={onAdd}
          variant="contained"
          color="primary"
          className={classes.button}
        >
          Ajouter
        </Button>
      </div>
    </div>
  );
};

export default List;
