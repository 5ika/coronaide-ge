import React from "react";
import { ThemeProvider, createMuiTheme } from "@material-ui/core/styles";
import { lightGreen } from "@material-ui/core/colors";
import Router from "./Router";

const theme = createMuiTheme({
  palette: {
    primary: lightGreen
  }
});

function App() {
  return (
    <ThemeProvider theme={theme}>
      <Router />
    </ThemeProvider>
  );
}

export default App;
