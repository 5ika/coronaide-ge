import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

// Pages
import Home from "./pages/Home";
import NeedHelp from "./pages/NeedHelp";

const AppRouter = () => {
  return (
    <Router>
      <Switch>
        <Route path="/needHelp" component={NeedHelp} />
        <Route path="/" component={Home} />
      </Switch>
    </Router>
  );
};

export default AppRouter;
