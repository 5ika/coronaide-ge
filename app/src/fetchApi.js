const { REACT_APP_API_URL = "https://coronaide.5ika.org" } = process.env;

const fetchAPI = async (url, params = {}) => {
  let {
    token,
    body = null,
    headers = {},
    isJSON = true, // If true, body is sent as JSON
    ...options
  } = params;

  // Set headers
  options = {
    ...options,
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      ...headers
    }
  };
  // Set authentication header if token is provided
  if (token)
    options.headers = { ...options.headers, Authorization: `Bearer ${token}` };

  // Set body and content type
  if (body) options.body = isJSON ? JSON.stringify(body) : body;
  if (!isJSON) delete options.headers["Content-Type"];

  // Send request
  try {
    const responseRaw = await fetch(REACT_APP_API_URL + url, options);
    const response = await responseRaw.json();
    if (response.hasOwnProperty("error")) throw response;
    return response;
  } catch (response) {
    console.error(response);
    throw new Error(response.message ?? response.error);
  }
};

export default fetchAPI;
