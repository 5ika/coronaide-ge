import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Typo from "@material-ui/core/Typography";
import LogoUrl from "../assets/logo_asg.png";
import pictoUrl from "../assets/coronaide.png";
import api from "../fetchApi";

import pdfLink from "../assets/Recommandations_Covid-19_scouts1.pdf";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    minHeight: "100vh",
    maxWidth: "100vw",
    overflow: "hidden",
    alignItems: "center",
    justifyContent: "center",
  },
  wrapper: {
    maxWidth: "100%",
    width: "640px",
    padding: "1rem",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  count: {
    textAlign: "center",
    marginTop: "1rem",
    marginBottom: "2rem",
  },
  logo: {
    maxWidth: "60%",
    marginTop: "1rem",
    marginBottom: "4rem",
  },
  picto: {
    width: "100%",
  },
  title: {
    textAlign: "center",
    fontSize: "3.5rem",
    marginBottom: "1rem",
  },
  subtitle: {
    textAlign: "center",
    fontSize: "2rem",
    marginBottom: "2rem",
  },
  description: {
    marginBottom: "2rem",
  },
  actions: {
    display: "flex",
    justifyContent: "center",
    marginBottom: "2rem",
  },
  button: {
    width: "100%",
    fontSize: "1.5rem",
  },
  footprint: {
    textAlign: "center",
    color: "grey",
  },
  link: {
    color: "black",
  },
}));

const Home = ({ history }) => {
  const classes = useStyles();
  const [count, setCount] = useState();

  useEffect(() => {
    const getCount = async () => {
      try {
        const result = await api("/courses/count");
        setCount(result);
      } catch (error) {
        console.error(error);
      }
    };
    getCount();
  }, []);

  return (
    <div className={classes.root}>
      <div className={classes.wrapper}>
        <Typo variant="h1" className={classes.title}>
          Coronaide Genève
        </Typo>
        <Typo variant="h2" className={classes.subtitle}>
          Les scouts genevois se mobilisent pour la pandémie de Covid-19
        </Typo>
        <Typo className={classes.description}>
          Compte tenu de la situation sanitaire actuelle, nous, les scouts du
          Canton de Genève, nous rendons disponibles pour aider les personnes
          âgées et les personnes à risque à faire leurs courses, chercher leurs
          médicaments ou toute autre activité ayant lieu dans l’espace public
          durant la durée de la pandémie.
        </Typo>

        <div className={classes.actions}>
          <Button
            variant="contained"
            color="primary"
            className={classes.button}
            onClick={() => history.push("/needHelp")}
          >
            J'ai besoin d'aide
          </Button>
        </div>
        <Typo className={classes.description}>
          <h4>Comment ça fonctionne ?</h4>
          <img src={pictoUrl} alt="Fonctionnement" className={classes.picto} />
          <ol>
            <li>Vous formulez une demande sur ce site</li>
            <li>
              Votre demande est relayée aux scouts de votre commune (ou aux plus
              proches)
            </li>
            <li>
              Les scouts s'organisent pour aller chercher votre besoin et vous
              l'amènent chez vous
            </li>
          </ol>
          Ce service de livraison est offert gratuitement et solidairement par
          les scouts genevois pendant la période de pandémie. Vous devez
          néanmoins payer vos achats à la livraison.
        </Typo>
        {count && (
          <div className={classes.count}>
            <Typo variant="h3">{count}</Typo>
            <Typo variant="h5">demandes reçues</Typo>
          </div>
        )}
        <Typo className={classes.description}>
          <h4>Recommandations</h4>
          <p>
            Ces recommandations sont fournies par le médecin cantonal et suivent
            <a
              href="https://ofsp-coronavirus.ch/"
              target="_blank"
              rel="noopener noreferrer"
            >
              {" "}
              les règles de l'OFSP
            </a>
            .
          </p>
          <ul>
            <li>
              Tout contact entre livreur.euse.s et demandeur.euse.s doit être
              évité. Les courses sont déposées sur le pas de la porte. Une
              distance de 2 mètres entre les personnes doit être respectée.
            </li>
            <li>
              Pas de possibilité de paiement en cash, uniquement par virement ou
              BVR communiqué lors de la livraison.
            </li>
            <li>
              Les livreur.euse.s sont prié.e.s de se laver les mains avant et
              après avoir fait les courses.
            </li>
            <li>Interdiction de se rassembler à plus de 5 personnes.</li>
            <li>
              Lors du dépôt de courses, ne pas serrer la main ni entrer ou
              inviter dans le domicile.
            </li>
            <li>
              Il est fortement conseillé aux demandeur.euse.s de se laver les
              mains après réception des sacs et éventuellement de désinfecter
              les anses de ces derniers.
            </li>
          </ul>
          Pour toute question vis-à-vis des mesures d'hygiène que nous prenons
          durant nos livraisons, veuillez{" "}
          <a
            href={pdfLink}
            target="_blank"
            rel="noopener noreferrer"
            className={classes.link}
          >
            consulter ce document
          </a>
          .
        </Typo>
        <a
          href="https://scouts-geneve.ch/"
          target="_blank"
          className={classes.logo}
          rel="noopener noreferrer"
        >
          <img src={LogoUrl} alt="Logo ASG" style={{ width: "100%" }} />
        </a>
        <Typo variant="caption" className={classes.footprint}>
          Si vous souhaitez mettre en place ce site pour une autre région ou un
          autre groupe de personnes, contactez le développeur à{" "}
          <a
            href="mailto:tim@human-apps.ch"
            className={classes.link}
            target="_blank"
            rel="noopener noreferrer"
          >
            tim@human-apps.ch
          </a>
          .
        </Typo>
      </div>
    </div>
  );
};

export default Home;
