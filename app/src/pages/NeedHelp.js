import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Typo from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import { useForm } from "react-hook-form";
import TextField from "@material-ui/core/TextField";
import { Link } from "react-router-dom";
import api from "../fetchApi";
import PostCodes from "../components/PostCodes";

const useStyles = makeStyles(theme => ({
  root: {
    display: "flex",
    minHeight: "100vh",
    maxWidth: "100vw",
    overflow: "hidden",
    alignItems: "center",
    justifyContent: "center"
  },
  wrapper: {
    maxWidth: "100%",
    width: "640px",
    padding: "1rem"
  },
  title: {
    textAlign: "center",
    fontSize: "3.5rem",
    marginBottom: "1rem"
  },
  form: {
    marginTop: "2rem"
  },
  button: {
    width: "100%",
    fontSize: "1rem",
    marginTop: "2rem"
  },
  sent: {
    marginTop: "2rem",
    textAlign: "center",
    color: theme.palette.primary.dark,
    fontSize: "1.2rem"
  },
  link: {
    textAlign: "center",
    color: theme.palette.grey[800],
    width: "100%",
    marginTop: "3rem"
  }
}));

const inputProps = { fullWidth: true, variant: "outlined" };

const NeedHelp = () => {
  const classes = useStyles();
  const { register, handleSubmit, errors } = useForm();
  const [sent, setSent] = useState(false);
  const [commune, setCommune] = useState("");
  const [np, setNp] = useState("");

  const onSubmit = async data => {
    if (!Object.keys(errors).length === 0) return;
    try {
      const result = await api("/courses", {
        method: "POST",
        body: { ...data, commune, np }
      });
      console.log({ result });
    } catch (error) {
      console.error(error);
    }
    setSent(true);
  };

  if (sent)
    return (
      <div>
        <Typo className={classes.sent}>
          C'est envoyé! Nous mettons des scouts sur le coup au plus vite.
        </Typo>
        <Link to="/" className={classes.link} style={{ display: "block" }}>
          Retour sur la page d'accueil
        </Link>
      </div>
    );

  return (
    <div className={classes.root}>
      <div className={classes.wrapper}>
        <Typo variant="h1" className={classes.title}>
          Coronaide Genève
        </Typo>
        <Typo className={classes.tip}>
          Pour vous aider au mieux, nous avons besoin que vous fournissiez ces
          quelques informations. Ces données seront uniquement utilisées dans le
          cadre de l'aide à domicile fourni par les scouts Genevois.
          <br />
          <br />
          Pour gérer le flux de demandes, nous effectuons un relevé chaque jour
          à 17:00.
        </Typo>
        <div className={classes.form}>
          <form onSubmit={handleSubmit(onSubmit)}>
            <Grid container spacing={2}>
              <Grid item xs={12} md={6}>
                <TextField
                  label="Prénom"
                  name="firstname"
                  inputRef={register({ required: true })}
                  error={!!errors?.firstname}
                  {...inputProps}
                />
              </Grid>
              <Grid item xs={12} md={6}>
                <TextField
                  label="Nom"
                  name="lastname"
                  error={!!errors?.lastname}
                  inputRef={register({ required: true })}
                  {...inputProps}
                />
              </Grid>
              <Grid item xs={12} md={6}>
                <TextField
                  label="Téléphone"
                  name="phone"
                  error={!!errors?.phone}
                  inputRef={register({ required: true })}
                  helperText="Pour vous appeler si nécessaire"
                  type="tel"
                  {...inputProps}
                />
              </Grid>
              <Grid item xs={12} md={6}>
                <PostCodes
                  value={commune}
                  setValue={setCommune}
                  setNp={setNp}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  label="Votre adresse"
                  name="address"
                  inputRef={register({ required: true })}
                  helperText="Merci de donner l'adresse complète pour vous trouver avec n° étage et code porte si nécessaire"
                  multiline
                  error={!!errors?.address}
                  rows={4}
                  {...inputProps}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  label="De quoi avez-vous besoin ?"
                  name="comment"
                  inputRef={register}
                  {...inputProps}
                  multiline
                  rows={4}
                />
              </Grid>
            </Grid>
            <Button
              variant="contained"
              color="primary"
              className={classes.button}
              type="submit"
            >
              Envoyer ma demande
            </Button>
          </form>
        </div>
        <Link to="/" className={classes.link} style={{ display: "block" }}>
          Retour sur la page d'accueil
        </Link>
      </div>
    </div>
  );
};

export default NeedHelp;
