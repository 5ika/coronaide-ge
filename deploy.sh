#!/bin/bash

helm upgrade --install coronaide k8s/* \
  -n coronaide --cleanup-on-fail \
  --set strapi.image=registry.gitlab.com/5ika/coronaide-ge:v0.5 \
  --set strapi.host=coronaide.5ika.org
