#!/bin/bash

docker rm -f mongo-coronaide || true
docker run -d --name mongo-coronaide -p 27017:27017 -v $PWD/.db:/data/db -v $PWD/.dump:/dump mongo