"use strict";
const axios = require("axios");
const moment = require('moment');

const TOKEN_TELEGRAM = "1146716370:AAHjT5dS7Pu7W5Tx0z24ijBDCA6UlkhzB1I";

const Airtable = require('airtable');
Airtable.configure({
    endpointUrl: 'https://api.airtable.com',
    apiKey: 'keyvR32AKUMoNLVXG'
});
const base = Airtable.base('appzLasf64gydxVcT');

/**
 * Lifecycle callbacks for the `course` model.
 */

module.exports = {
  // Before saving a value.
  // Fired before an `insert` or `update` query.
  // beforeSave: async (model, attrs, options) => {},

  // After saving a value.
  // Fired after an `insert` or `update` query.
  // afterSave: async (model, response, options) => {},

  // Before fetching a value.
  // Fired before a `fetch` operation.
  // beforeFetch: async (model, columns, options) => {},

  // After fetching a value.
  // Fired after a `fetch` operation.
  // afterFetch: async (model, response, options) => {},

  // Before fetching all values.
  // Fired before a `fetchAll` operation.
  // beforeFetchAll: async (model, columns, options) => {},

  // After fetching all values.
  // Fired after a `fetchAll` operation.
  // afterFetchAll: async (model, response, options) => {},

  // Before creating a value.
  // Fired before an `insert` query.
  // beforeCreate: async (model, attrs, options) => {},

  // After creating a value.
  // Fired after an `insert` query.
  afterCreate: async (model, attrs, options) => {
    // console.log({ model });

    try {
      const [commune] = await strapi.services.commune.find({ np: model.np });

      const text = `Nouvelle demande:
${model.firstname} ${model.lastname} - ${model.phone}

**Commune:** ${commune ? commune.name : "Commune inconnue"} (${model.np})

**Adresse:**
${model.address}

**Demande:**
${model.comment}

**Groupe:** ${
        commune && commune.groupe
          ? `${commune.groupe.name} (${commune.groupe.leader})`
          : "Pas de groupe pour la commune"
      }
`;

      // Envoi à Telegram
      await axios.post(
        `https://api.telegram.org/bot${TOKEN_TELEGRAM}/sendMessage`,
        {
          chat_id: "5926939",
          text,
          parse_mode: "Markdown"
        }
      );

      await base('Courses').create([
      {
       "fields": {
		"prénom": model.firstname,
		"nom": model.lastname,
		"adresse": model.address,
		"besoin": model.comment,
		"np": model.np,
		"tel": model.phone,
		"date": moment().format("YYYY-MM-DD"),
		"groupe": commune && commune.groupe ? commune.groupe.name : "Aucun groupe"
	}
      }]);

      // Envoi à Discord
//      await axios.post(
//        `https://discordapp.com/api/webhooks/690581578186752020/_4sNjA6cPrUJVfH6X-VnLVGfO7iROTmm3_5hWtw4T8Xavm7j8o2kzh-fbQHWcWDOc0hF`,
//        {
//          content: text
//        }
//      );
    } catch (error) {
      console.log({error});
      await axios.post(
        `https://api.telegram.org/bot${TOKEN_TELEGRAM}/sendMessage`,
        {
          chat_id: "5926939",
          text: "Il y a eu un bug serveur lors du traitement d'une demande.",
          parse_mode: "Markdown"
        }
      );
    }
  }

  // Before updating a value.
  // Fired before an `update` query.
  // beforeUpdate: async (model, attrs, options) => {},

  // After updating a value.
  // Fired after an `update` query.
  // afterUpdate: async (model, attrs, options) => {},

  // Before destroying a value.
  // Fired before a `delete` query.
  // beforeDestroy: async (model, attrs, options) => {},

  // After destroying a value.
  // Fired after a `delete` query.
  // afterDestroy: async (model, attrs, options) => {}
};
